package hu.ponte.hr.exceptions;

/**
 * It is thrown when error occurs in {@code FileSystemStorageService}.
 */
public class FileSystemStorageException extends RuntimeException {

  public FileSystemStorageException(String message) {
    super(message);
  }

  public FileSystemStorageException(String message, Throwable cause) {
    super(message, cause);
  }
}