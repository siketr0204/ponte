package hu.ponte.hr.exceptions;

/**
 * It is thrown when a file is not image.
 */
public class FileIsNotImageException extends RuntimeException {

  private static final String MESSAGE = "File is not an image: %s";

  public FileIsNotImageException(String filename) {
    super(String.format(MESSAGE, filename));
  }
}
