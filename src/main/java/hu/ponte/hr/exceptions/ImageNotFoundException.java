package hu.ponte.hr.exceptions;

/**
 * It is thrown when an image with a given id is not found in the system.
 */
public class ImageNotFoundException extends RuntimeException {

  private static final String MESSAGE_NOT_FOUND = "Image is not found with the following id: %s";

  public ImageNotFoundException(String imageId) {
    super(String.format(MESSAGE_NOT_FOUND, imageId));
  }
}
