package hu.ponte.hr.exceptions;

/**
 * It is thrown when exception occurs in {@code ImageService}.
 */
public class ImageServiceException extends RuntimeException {

  public ImageServiceException(Exception e) {
    super(e);
  }
}
