package hu.ponte.hr.exceptions;

/**
 * It is thrown when content type of a file is unknown.
 */
public class ContentTypeUnknownException extends RuntimeException {

  private static final String MESSAGE = "Content type of file is unknown: %s";

  public ContentTypeUnknownException(String filename) {
    super(String.format(MESSAGE, filename));
  }
}
