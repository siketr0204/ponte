package hu.ponte.hr.exceptions;

/**
 * It is thrown when error occurs in {@code SignService}.
 */
public class SignServiceException extends RuntimeException {

  public SignServiceException(Exception e) {
    super(e);
  }
}
