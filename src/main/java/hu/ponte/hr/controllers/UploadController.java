package hu.ponte.hr.controllers;

import hu.ponte.hr.domain.RestResponse;
import hu.ponte.hr.services.ImageService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller of uploading images to system.
 */
@Component
@RequestMapping("api/file")
public class UploadController {

  private final ImageService imageService;

  public UploadController(ImageService imageService) {
    this.imageService = imageService;
  }

  /**
   * @param file File is uploaded for saving.
   * @return It returns {@code RestResponse} with OK result.
   */
  @PostMapping("post")
  @ResponseBody
  public RestResponse handleFormUpload(@RequestParam("file") MultipartFile file) {
    imageService.saveNewImage(file);
    return RestResponse.OK;
  }
}
