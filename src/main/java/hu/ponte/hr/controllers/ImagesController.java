package hu.ponte.hr.controllers;

import hu.ponte.hr.dto.ImageDto;
import hu.ponte.hr.services.ImageService;
import java.util.List;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller of images.
 */
@RestController()
@RequestMapping("api/images")
public class ImagesController {

  private final ImageService imageService;

  public ImagesController(ImageService imageService) {
    this.imageService = imageService;
  }

  /**
   * List all images of the system.
   *
   * @return All images of the system.
   */
  @GetMapping("meta")
  public List<ImageDto> listImages() {
    return imageService.getImages();
  }

  /**
   * Download one image with the specified id.
   *
   * @param id id of the image.
   * @return it returns an {@code ResponseEntity<InputStreamResource>} which represent an image.
   */
  @GetMapping("preview/{id}")
  public ResponseEntity<InputStreamResource> downloadImage(@PathVariable("id") String id) {
    return imageService.getImageAsResource(id);
  }
}
