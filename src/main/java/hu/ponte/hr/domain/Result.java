package hu.ponte.hr.domain;

/**
 * Result of an api operation.
 */
public enum Result {
  OK,
  ERROR
}
