package hu.ponte.hr.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * Response class of api rest endpoints.
 */
@Builder
@AllArgsConstructor
@Getter
public class RestResponse {

  public static final RestResponse OK = RestResponse.builder().result(Result.OK).build();

  private final Result result;
  @JsonInclude(Include.NON_NULL)
  private final Object additionalInfo;

}
