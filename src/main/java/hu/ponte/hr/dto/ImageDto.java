package hu.ponte.hr.dto;

import lombok.Data;

/**
 * Dto class of {@code ImageEntity}.
 */
@Data
public class ImageDto {

  private long size;
  private String id;
  private String name;
  private String mimeType;
  private String digitalSign;
}
