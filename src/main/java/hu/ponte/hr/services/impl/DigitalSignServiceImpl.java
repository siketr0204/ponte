package hu.ponte.hr.services.impl;

import hu.ponte.hr.exceptions.SignServiceException;
import hu.ponte.hr.services.DigitalSignService;
import java.io.FileInputStream;
import java.security.Signature;
import java.util.Base64;
import org.apache.commons.ssl.PKCS8Key;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class DigitalSignServiceImpl implements DigitalSignService {

  private static final String RSA_SIGNATURE_ALGORITHM = "SHA256withRSA";
  @Value("classpath:config/keys/key.private")
  private Resource privateKey;

  public String sign(byte[] fileData) {
    try {

      PKCS8Key pkcs8 =
          new PKCS8Key(new FileInputStream(privateKey.getFile()), "".toCharArray());

      Signature signature = Signature.getInstance(RSA_SIGNATURE_ALGORITHM);
      signature.initSign(pkcs8.getPrivateKey());
      signature.update(fileData);
      return Base64.getEncoder().encodeToString(signature.sign());
    } catch (Exception e) {
      throw new SignServiceException(e);
    }
  }
}
