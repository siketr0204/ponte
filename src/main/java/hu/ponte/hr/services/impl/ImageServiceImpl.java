package hu.ponte.hr.services.impl;

import hu.ponte.hr.dto.ImageDto;
import hu.ponte.hr.entities.ImageEntity;
import hu.ponte.hr.exceptions.ContentTypeUnknownException;
import hu.ponte.hr.exceptions.FileIsNotImageException;
import hu.ponte.hr.exceptions.ImageNotFoundException;
import hu.ponte.hr.exceptions.ImageServiceException;
import hu.ponte.hr.mappers.ImageMapper;
import hu.ponte.hr.repositories.ImageRepository;
import hu.ponte.hr.services.DigitalSignService;
import hu.ponte.hr.services.FileSystemStorageService;
import hu.ponte.hr.services.ImageService;
import java.io.IOException;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageServiceImpl implements ImageService {

  private static final String CONTENT_TYPE_IMAGE_PREFIX = "image";
  private final ImageRepository imageRepository;
  private final ImageMapper imageMapper;
  private final FileSystemStorageService fileSystemStorageService;
  private final DigitalSignService digitalSignService;

  public ImageServiceImpl(ImageRepository imageRepository, ImageMapper imageMapper, FileSystemStorageService fileSystemStorageService,
                          DigitalSignService digitalSignService) {
    this.imageRepository = imageRepository;
    this.imageMapper = imageMapper;
    this.fileSystemStorageService = fileSystemStorageService;
    this.digitalSignService = digitalSignService;
  }

  @Override
  public List<ImageDto> getImages() {
    return imageMapper.entitiesToDtos(imageRepository.findAll());
  }

  @Transactional
  @Override
  public void saveNewImage(MultipartFile file) {
    checkIfFileIsImage(file);
    ImageEntity imageEntity = imageRepository.save(createImageFromFile(file));
    fileSystemStorageService.store(file, getFilename(file.getOriginalFilename(), imageEntity.getId()));
  }

  private void checkIfFileIsImage(MultipartFile file) {
    if (file.getContentType() == null) {
      throw new ContentTypeUnknownException(file.getOriginalFilename());
    } else if (!file.getContentType().startsWith(CONTENT_TYPE_IMAGE_PREFIX)) {
      throw new FileIsNotImageException(file.getOriginalFilename());
    }
  }

  @Override
  public ResponseEntity<InputStreamResource> getImageAsResource(String id) {
    return imageRepository.findById(id)
                          .map(image -> loadAsResource(image, id))
                          .orElseThrow(() -> new ImageNotFoundException(id));
  }

  private ResponseEntity<InputStreamResource> loadAsResource(ImageEntity image, String id) {
    Resource resource = fileSystemStorageService.loadAsResource(getFilename(image.getName(), id));

    try {
      return ResponseEntity.ok()
                           .contentLength(resource.contentLength())
                           .contentType(MediaType.parseMediaType(image.getMimeType()))
                           .body(new InputStreamResource(resource.getInputStream()));
    } catch (Exception e) {
      throw new ImageServiceException(e);
    }
  }

  private String getFilename(String originalFilename, String id) {
    return id + FilenameUtils.getExtension(originalFilename);
  }

  private ImageEntity createImageFromFile(MultipartFile file) {
    try {
      return ImageEntity.builder()
                        .name(file.getOriginalFilename())
                        .size(file.getSize())
                        .mimeType(file.getContentType())
                        .digitalSign(digitalSignService.sign(file.getBytes()))
                        .build();
    } catch (IOException e) {
      throw new ImageServiceException(e);
    }
  }
}
