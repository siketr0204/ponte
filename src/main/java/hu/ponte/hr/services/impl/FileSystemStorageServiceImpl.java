package hu.ponte.hr.services.impl;

import hu.ponte.hr.exceptions.FileSystemStorageException;
import hu.ponte.hr.services.FileSystemStorageService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileSystemStorageServiceImpl implements FileSystemStorageService, InitializingBean {

  private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemStorageServiceImpl.class);
  private static final String SYSTEM_PROPERTY_CATALINA_BASE = "catalina.base";
  private static final String SLASH = "/";
  private static final String EXCEPTION_FAILED_STORE = "Failed to store file %s";
  private static final String EXCEPTION_EMPTY_FILE = "Failed to store empty file %s";
  private static final String EXCEPTION_CANNOT_READ_FILE = "Cannot not read file: %s";
  private static final String INFO_FILE_STORAGE_CREATED = "File storage is created: %s";
  private static final String EXCEPTION_CANNOT_INITIALIZE_STORAGE = "Cannot not initialize storage";
  private static final String UPLOAD_BASE_PATH = "${upload.base.path}";
  private final Path rootLocation;

  @Autowired
  public FileSystemStorageServiceImpl(@Value(value = UPLOAD_BASE_PATH) String uploadLocation) {
    rootLocation = getRootLocation(uploadLocation);
  }

  private Path getRootLocation(String uploadLocation) {
    return Optional.ofNullable(System.getProperty(SYSTEM_PROPERTY_CATALINA_BASE))
            .map(catalinaBase -> Paths.get(catalinaBase + SLASH + uploadLocation))
            .orElse(Paths.get(uploadLocation));
  }

  @Override
  public void store(MultipartFile file, String filename) {
    try {
      checkFileEmpty(file);
      Files.copy(file.getInputStream(), rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException e) {
      throw new FileSystemStorageException(String.format(EXCEPTION_FAILED_STORE, file.getName()), e);
    }
  }

  @Override
  public Resource loadAsResource(String filename) {
    try {
      Path file = rootLocation.resolve(filename);
      Resource resource = new UrlResource(file.toUri());
      if (resource.exists() && resource.isReadable()) {
        return resource;
      } else {
        throw new FileSystemStorageException(String.format(EXCEPTION_CANNOT_READ_FILE, filename));
      }
    } catch (MalformedURLException e) {
      throw new FileSystemStorageException(String.format(EXCEPTION_CANNOT_READ_FILE, filename), e);
    }
  }

  @Override
  public void afterPropertiesSet() {
    try {
      if (!Files.exists(rootLocation)) {
        Files.createDirectories(rootLocation);
        LOGGER.info(String.format(INFO_FILE_STORAGE_CREATED, rootLocation));
      }
    } catch (IOException e) {
      throw new FileSystemStorageException(EXCEPTION_CANNOT_INITIALIZE_STORAGE, e);
    }
  }

  private void checkFileEmpty(MultipartFile file) {
    if (file.isEmpty()) {
      throw new FileSystemStorageException(String.format(EXCEPTION_EMPTY_FILE, file.getOriginalFilename()));
    }
  }
}
