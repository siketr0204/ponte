package hu.ponte.hr.services;

import hu.ponte.hr.dto.ImageDto;
import java.util.List;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 * It manages image related operations.
 */
public interface ImageService {

  /**
   * It loads all images of the system.
   *
   * @return Images of the system.
   */
  List<ImageDto> getImages();

  /**
   * It saves an image file to the system.
   *
   * @param image Image file to save
   */
  void saveNewImage(MultipartFile image);

  /**
   * It loads an image from the system.
   *
   * @param id id of the image
   * @return Image response
   */
  ResponseEntity<InputStreamResource> getImageAsResource(String id);
}
