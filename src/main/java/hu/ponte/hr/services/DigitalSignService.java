package hu.ponte.hr.services;

/**
 * It digitally signs of files.
 */
public interface DigitalSignService {

  /**
   * It signs the file.
   *
   * @param fileData byte array repesentation of a file
   * @return the sign result
   */
  String sign(byte[] fileData);
}
