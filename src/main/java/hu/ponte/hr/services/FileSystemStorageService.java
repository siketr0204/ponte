package hu.ponte.hr.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * It stores files to or loads files from file system.
 */
public interface FileSystemStorageService {

  /**
   * It stores file to file system.
   *
   * @param file     File to store
   * @param filename name of the file
   */
  void store(MultipartFile file, String filename);

  /**
   * It loads a file from the file system.
   *
   * @param filename name of the file
   * @return resource representation of the file
   */
  Resource loadAsResource(String filename);
}
