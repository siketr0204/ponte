package hu.ponte.hr.mappers;

import hu.ponte.hr.dto.ImageDto;
import hu.ponte.hr.entities.ImageEntity;
import org.mapstruct.Mapper;

/**
 * Mapper of {@code ImageEntity} and {@code ImageDto}
 */
@Mapper(componentModel = "spring")
public interface ImageMapper extends BaseMapper<ImageEntity, ImageDto> {

}
