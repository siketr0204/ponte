package hu.ponte.hr.mappers;

import java.util.List;

/**
 * Base interface for mapping between Dto and Entity classes.
 *
 * @param <ENTITY> Entity for mapping.
 * @param <DTO>    Dto for mapping.
 */
public interface BaseMapper<ENTITY, DTO> {

  DTO entityToDto(ENTITY entity);

  List<DTO> entitiesToDtos(List<ENTITY> entities);

  ENTITY dtoToEntity(DTO dto);

  List<ENTITY> dtosToEntities(List<DTO> dtos);
}