package hu.ponte.hr.repositories;

import hu.ponte.hr.entities.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository of images.
 */
@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, String> {

}
