package hu.ponte.hr.configs;

import hu.ponte.hr.domain.RestResponse;
import hu.ponte.hr.domain.Result;
import hu.ponte.hr.exceptions.FileIsNotImageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Global exception handling class.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  /**
   * It produces error response for default exception.
   *
   * @param e Thrown exception
   * @return It returns with {@code ResponseEntity<RestResponse>} which contains the exception message
   */
  @ExceptionHandler(value = Exception.class)
  @ResponseBody
  public ResponseEntity<RestResponse> defaultErrorHandler(Exception e) {
    LOGGER.error(e.getMessage(), e);
    return new ResponseEntity<>(RestResponse.builder()
                                            .result(Result.ERROR)
                                            .additionalInfo(e.getMessage())
                                            .build(),
                                HttpStatus.INTERNAL_SERVER_ERROR
    );
  }

  /**
   * It produces error response for {@code FileIsNotImageException} exception.
   *
   * @param e Thrown exception
   * @return It returns with {@code ResponseEntity<RestResponse>} which contains the exception message
   */
  @ExceptionHandler(value = FileIsNotImageException.class)
  @ResponseBody
  public ResponseEntity<RestResponse> fileIsNotImageExceptionErrorHandler(FileIsNotImageException e) {
    LOGGER.error(e.getMessage(), e);
    return new ResponseEntity<>(RestResponse.builder()
                                            .result(Result.ERROR)
                                            .additionalInfo(e.getMessage())
                                            .build(),
                                HttpStatus.BAD_REQUEST
    );
  }
}
