package hu.ponte.hr;

import hu.ponte.hr.services.DigitalSignService;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest()
public class SignIntegrationTest {

  private static final String SIGN_CAT_JPG = "XYZ+wXKNd3Hpnjxy4vIbBQVD7q7i0t0r9tzpmf1KmyZAEUvpfV8AKQlL7us66rvd6eBzFlSaq5HGVZX2DYTxX1C5fJlh3T3QkVn2zKOfPHDWWItdXkrccCHVR5HFrpGuLGk7j7XKORIIM+DwZKqymHYzehRvDpqCGgZ2L1Q6C6wjuV4drdOTHps63XW6RHNsU18wHydqetJT6ovh0a8Zul9yvAyZeE4HW7cPOkFCgll5EZYZz2iH5Sw1NBNhDNwN2KOxrM4BXNUkz9TMeekjqdOyyWvCqVmr5EgssJe7FAwcYEzznZV96LDkiYQdnBTO8jjN25wlnINvPrgx9dN/Xg==";
  private static final String SIGN_BUZZ_JPG = "tsLUqkUtzqgeDMuXJMt1iRCgbiVw13FlsBm2LdX2PftvnlWorqxuVcmT0QRKenFMh90kelxXnTuTVOStU8eHRLS3P1qOLH6VYpzCGEJFQ3S2683gCmxq3qc0zr5kZV2VcgKWm+wKeMENyprr8HNZhLPygtmzXeN9u6BpwUO9sKj7ImBvvv/qZ/Tht3hPbm5SrDK4XG7G0LVK9B8zpweXT/lT8pqqpYx4/h7DyE+L5bNHbtkvcu2DojgJ/pNg9OG+vTt/DfK7LFgCjody4SvZhSbLqp98IAaxS9BT6n0Ozjk4rR1l75QP5lbJbpQ9ThAebXQo+Be4QEYV/YXf07WXTQ==";
  private static final String SIGN_RND_JPG = "lM6498PalvcrnZkw4RI+dWceIoDXuczi/3nckACYa8k+KGjYlwQCi1bqA8h7wgtlP3HFY37cA81ST9I0X7ik86jyAqhhc7twnMUzwE/+y8RC9Xsz/caktmdA/8h+MlPNTjejomiqGDjTGvLxN9gu4qnYniZ5t270ZbLD2XZbuTvUAgna8Cz4MvdGTmE3MNIA5iavI1p+1cAN+O10hKwxoVcdZ2M3f7/m9LYlqEJgMnaKyI/X3m9mW0En/ac9fqfGWrxAhbhQDUB0GVEl7WBF/5ODvpYKujHmBAA0ProIlqA3FjLTLJ0LGHXyDgrgDfIG/EDHVUQSdLWsM107Cg6hQg==";
  private static final String ASSERT_SIGNS_SHOULD_BE_EQUAL = "Signs should be equal.";
  private static final String KEY_CAT_JPG = "cat.jpg";
  private static final String KEY_BUZZ_JPG = "enhanced-buzz.jpg";
  private static final String KEY_RND_JPG = "rnd.jpg";
  private static final Map<String, String> FILES = new LinkedHashMap<String, String>() {
    {
      put(KEY_CAT_JPG, SIGN_CAT_JPG);
      put(KEY_BUZZ_JPG, SIGN_BUZZ_JPG);
      put(KEY_RND_JPG, SIGN_RND_JPG);
    }
  };
  @Value("classpath:images/cat.jpg")
  private Resource catJpg;
  @Value("classpath:images/enhanced-buzz.jpg")
  private Resource buzzJpg;
  @Value("classpath:images/rnd.jpg")
  private Resource rndJpg;
  @Autowired
  private DigitalSignService digitalSignService;

  @Test
  public void signCatJpgSignIsOK() throws IOException {
    String actual = digitalSignService.sign(loadResourceAsByteArray(catJpg));
    String expected = FILES.get(KEY_CAT_JPG);
    assertSignEquals(actual, expected);
  }

  @Test
  public void signBuzzJpgSignIsOK() throws IOException {
    String actual = digitalSignService.sign(loadResourceAsByteArray(buzzJpg));
    String expected = FILES.get(KEY_BUZZ_JPG);
    assertSignEquals(actual, expected);
  }

  @Test
  public void signRndJpgSignIsOK() throws IOException {
    String actual = digitalSignService.sign(loadResourceAsByteArray(rndJpg));
    String expected = FILES.get(KEY_RND_JPG);
    assertSignEquals(actual, expected);
  }

  private byte[] loadResourceAsByteArray(Resource resource) throws IOException {
    return IOUtils.toByteArray(new FileInputStream(resource.getFile()));
  }

  private void assertSignEquals(String actual, String expected) {
    Assert.assertEquals(ASSERT_SIGNS_SHOULD_BE_EQUAL, expected, actual);
  }
}
