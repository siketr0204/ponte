package hu.ponte.hr;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ImagesIntegrationTests {

  public static final String FILE_NAME = "file";
  private static final String SIGN_CAT_JPG = "XYZ+wXKNd3Hpnjxy4vIbBQVD7q7i0t0r9tzpmf1KmyZAEUvpfV8AKQlL7us66rvd6eBzFlSaq5HGVZX2DYTxX1C5fJlh3T3QkVn2zKOfPHDWWItdXkrccCHVR5HFrpGuLGk7j7XKORIIM+DwZKqymHYzehRvDpqCGgZ2L1Q6C6wjuV4drdOTHps63XW6RHNsU18wHydqetJT6ovh0a8Zul9yvAyZeE4HW7cPOkFCgll5EZYZz2iH5Sw1NBNhDNwN2KOxrM4BXNUkz9TMeekjqdOyyWvCqVmr5EgssJe7FAwcYEzznZV96LDkiYQdnBTO8jjN25wlnINvPrgx9dN/Xg==";
  private static final String PATH_API_FILE_POST = "/api/file/post";
  private static final int IMAGE_ID = 4;
  private static final String PATH_IMAGES_META = "/api/images/meta";
  private static final String PATH_IMAGES_PREVIEW = "/api/images/preview/%s";
  @Value("classpath:images/cat.jpg")
  private Resource catJpg;
  @Value("classpath:simple.txt")
  private Resource textFile;
  @Autowired
  private MockMvc mockMvc;

  @Test
  public void listImagesShouldReturnThreeImages() throws Exception {
    this.mockMvc.perform(get(PATH_IMAGES_META))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].name").value("name1"))
                .andExpect(jsonPath("$[0].mimeType").value("image/type1"))
                .andExpect(jsonPath("$[0].digitalSign").value("1"))
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].name").value("name2"))
                .andExpect(jsonPath("$[1].mimeType").value("image/type2"))
                .andExpect(jsonPath("$[1].digitalSign").value("2"))
                .andExpect(jsonPath("$[2].id").value("3"))
                .andExpect(jsonPath("$[2].name").value("name3"))
                .andExpect(jsonPath("$[2].mimeType").value("image/type3"))
                .andExpect(jsonPath("$[2].digitalSign").value("3"));

  }

  @Test
  public void uploadAndDownloadImageShouldWork() throws Exception {
    performImageUpload();
    performImagePreview();
    performGetImages();
  }

  @Test
  public void uploadShouldFailIfFileIsNotImage() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.multipart(PATH_API_FILE_POST).file(getTextMockMultipartFile()))
           .andDo(print())
           .andExpect(status().isBadRequest())
           .andExpect(jsonPath("$.result").value("ERROR"))
           .andExpect(jsonPath("$.additionalInfo").value("File is not an image: simple.txt"));
  }

  private void performGetImages() throws Exception {
    this.mockMvc.perform(get(PATH_IMAGES_META))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$[3].id").value("4"))
                .andExpect(jsonPath("$[3].name").value("cat.jpg"))
                .andExpect(jsonPath("$[3].mimeType").value("image/jpeg"))
                .andExpect(jsonPath("$[3].digitalSign").value(SIGN_CAT_JPG));
  }

  private void performImagePreview() throws Exception {
    this.mockMvc.perform(get(String.format(PATH_IMAGES_PREVIEW, IMAGE_ID)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentTypeCompatibleWith(MediaType.IMAGE_JPEG));
  }

  private void performImageUpload() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.multipart(PATH_API_FILE_POST).file(getJpegMockMultipartFile()))
           .andExpect(status().is2xxSuccessful())
           .andExpect(jsonPath("$.result").value("OK"));
  }

  private MockMultipartFile getJpegMockMultipartFile() throws IOException {
    return new MockMultipartFile(FILE_NAME,
                                 catJpg.getFilename(),
                                 MediaType.IMAGE_JPEG_VALUE,
                                 catJpg.getInputStream()
    );
  }

  private MockMultipartFile getTextMockMultipartFile() throws IOException {
    return new MockMultipartFile(FILE_NAME,
                                 textFile.getFilename(),
                                 MediaType.TEXT_HTML_VALUE,
                                 textFile.getInputStream()
    );
  }
}

